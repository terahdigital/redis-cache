<?php declare(strict_types=1);

namespace Terah\RedisCache\Test;

require_once __DIR__ . '/../../../autoload.php';

use PHPUnit_Framework_TestCase;
use Terah\RedisCache\RedisCache;
use Redis;

class RedisCacheTest extends PHPUnit_Framework_TestCase
{
    protected ?RedisCache $redisCache   = null;

    protected string $key       = '/my-test-key';

    public function setUp() : void
    {
        $conf           = [
            'write'         => ['redis1.local'],
            'read'          => ['redis1.local'],
            'delete'        => ['redis1.local'],//, 'redis2.local', 'redis3.local', 'redis4.local', ],
        ];
        $servers        = [
            'read'          => [],
            'write'         => [],
            'delete'        => [],
            'all'           => [],
        ];
        foreach ( $conf as $type => $hosts )
        {
            foreach ( $hosts as $host )
            {
                if ( ! array_key_exists($host, $servers['all']) )
                {
                    $redis      = new Redis;
                    $redis->pconnect($host, 6379, 1);
                    $redis->auth(getenv('REDIS_PASSWORD'));
                    $servers['all'][$host] = $redis;
                }
                $servers[$type][$host] = $servers['all'][$host];
            }
        }

        $this->redisCache = new RedisCache($servers, 60 * 10, 'my_cache_test');
    }

    /**
     * @param string $host
     * @param int $port
     */
    protected function _getRedisInstance(string $host='127.0.0.1', int $port=6379)
    {
        $redis              = new Redis();
        $redis->connect($host, $port);
        $this->redisCache   = new RedisCache([[$redis]], 60 * 10, 'my_cache_test');
    }

    public function testSet()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $expiresAt      = time() + (60 * 30);
        $this->redisCache->set($this->key, $data, 60 * 30);
        $expires        = $this->redisCache->expires($this->key);
        $expires        = $expires->getTimestamp();
        $fetchedData    = $this->redisCache->get($this->key);
        static::assertEquals($expiresAt, $expires);
        static::assertEquals(json_encode($data), json_encode($fetchedData));
    }

    public function testGet()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $this->redisCache->set($this->key, $data, 60 * 30);
        $fetchedData   = $this->redisCache->get($this->key);
        static::assertEquals(json_encode($data), json_encode($fetchedData));
    }

    public function testExists()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $this->redisCache->set($this->key, $data, 60 * 30);
        $exists         = $this->redisCache->exists($this->key);
        static::assertTrue($exists);
    }

    public function testRemember()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $callback = function () use ($data) {
            return   $data;
        };
        $this->redisCache->remember($this->key, $callback, 60 * 10);
        $fetchedData   = $this->redisCache->remember($this->key, $callback, 60 * 10);
        static::assertEquals(json_encode($data), json_encode($fetchedData));
    }

    public function testDelete()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $this->redisCache->set($this->key, $data, 60 * 30);
        $exists         = $this->redisCache->exists($this->key);
        static::assertTrue($exists);
        $this->redisCache->delete($this->key);
        $exists         = $this->redisCache->exists($this->key);
        static::assertFalse($exists);
    }

    public function testflush()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $this->redisCache->set($this->key, $data, 60 * 30);
        $exists         = $this->redisCache->exists($this->key);
        static::assertTrue($exists);
        $this->redisCache->flush();
        $exists         = $this->redisCache->exists($this->key);
        static::assertFalse($exists);
    }

    public function testHierarchicalKeys()
    {
        $data           = ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $keys           = [
            '/asdf-asdc123/asdfccasd/asdfadsf',
            '/asdf-asdc123/asdfccasd/asdfadsf/casdcasdc',
            '/asdf-asdc123/asdfccasd/asdfadsf/zasde_-asd./asdfadsf',
            '/asdf-asdc123/asdfccasd/asdfadsf/asdfadsf-asdf-asdf',
        ];
        sort($keys);
        foreach ( $keys as $key )
        {
            $this->redisCache->set($key, $data, 600);
        }
        $allKeys    = $this->redisCache->allKeys();
        sort($allKeys);
        static::assertEquals(json_encode($keys), json_encode($allKeys));
        $this->redisCache->delete('/asdf-asdc123/');
        $allKeys    = $this->redisCache->allKeys();
        sort($allKeys);
        static::assertEquals(json_encode([]), json_encode($allKeys));
    }

    public function testExpires()
    {
        $data           =  ['asdfasdf' => 'acdasdcasd', 'cadcadscads' => 'cacee'];
        $this->redisCache->set($this->key, $data, 10);
        $exists         = $this->redisCache->exists($this->key);
        static::assertTrue($exists);
        sleep(11);
        $exists         = $this->redisCache->exists($this->key);
        static::assertFalse($exists);
    }

    public function tearDown()
    {
        $this->redisCache->flush();
    }
}
